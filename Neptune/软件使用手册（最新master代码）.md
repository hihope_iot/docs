# Neptune软件使用指南（最新master代码）



## 介绍

本文档介绍Neptune 开发板OpenHarmony最新master分支的开发环境搭建、版本编译构建、烧录、源码修改、调试验证等方法。通过学习，开发者会对Neptune开发流程有初步认识，并可上手业务开发。



## 参考网址

[W800官网](http://www.winnermicro.com/html/1/156/158/558.html)

[鸿蒙官网](https://device.harmonyos.com/cn/home)

[DevEco Device Tools介绍与下载](https://device.harmonyos.com/cn/ide)

[润和HiHope社区](https://gitee.com/hihopeorg)



## 开发环境

[1.环境搭建与编译](https://gitee.com/hihope_iot/docs/blob/master/Neptune/doc/1.%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E4%B8%8E%E7%BC%96%E8%AF%91.md)

[2.烧录固件](https://gitee.com/hihope_iot/docs/blob/master/Neptune/doc/2.%E7%83%A7%E5%BD%95%E5%9B%BA%E4%BB%B6.md)

[3.测试代码的使用](https://gitee.com/hihope_iot/docs/blob/master/Neptune/doc/3.%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81%E7%9A%84%E4%BD%BF%E7%94%A8.md)

[4.GPIO点灯和按键程序编写](https://gitee.com/hihope_iot/docs/blob/master/Neptune/doc/4.GPIO%E7%82%B9%E7%81%AF%E5%92%8C%E6%8C%89%E9%94%AE%E7%A8%8B%E5%BA%8F%E7%BC%96%E5%86%99.md)

[5.WiFi连接——STA模式](https://gitee.com/hihope_iot/docs/blob/master/Neptune/doc/WiFi%E8%BF%9E%E6%8E%A5%E2%80%94%E2%80%94STA%E6%A8%A1%E5%BC%8F.md)
