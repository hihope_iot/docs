# 3.测试代码的使用

这里我写了一套测试代码供大家使用，代码位于demo文件夹下面。

**第一步**

把 w800_demo 文件夹复制到 device\soc\winnermicro\wm800\board 路径。

**第二步**

修改device\soc\winnermicro\wm800\board\BUILD.gn，在module_group(module_name) 中加上w800_demo，至此就可以开始编译了

![image-20220505105929691](image/image-20220505105929691.png)

