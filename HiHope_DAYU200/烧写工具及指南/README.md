# HiHope-DAYU200烧录指导

- windows[烧写工具与驱动](https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97/windows)

- [烧写关键步骤说明](https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/docs/%E7%83%A7%E5%BD%95%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3.md)

- 烧写版本注意事项（重要）——近期固件有改动，烧写工具需要同步更新

  *Windows烧写方式：*

  1.烧写从每日构建版本master分支上 2022/01/18 及其以后的版本

  由于 DAYU200 master 的内核已经切换到了linux 5.10，同时增加了resouce.img，因此，请下载最新的windows烧写工具选择对应的镜像，即可正常烧写
  
  2.烧写从每日构建版本3.1-beta分支上 及master上 2022/01/18 以前的版本 
  
  将windows/linux_4_19.cfg重命名为config.cfg，重新打开烧录工具，选择对应的镜像，即可正常烧写

  3.master分支自 2022/05/09 开始，ramdisk从boot_linux.img中拆出，作为单独的镜像ramdisk.img进行烧写，与此同时，烧写工具配置文件config.cfg将随版本发布到镜像包中，便于分区镜像变更时烧写配置的更改，此后需要在烧写工具中导入镜像包中的config.cfg文件并按照相应配置选择镜像进行烧写。
  
  *Linux烧写方式：*
  
  1.拷贝 `HiHope_DAYU200/烧写工具及指南/linux/etc/udev/rules.d/85-rk3568.rules` 到系统 `/etc/udev/rules.d/`
  
  2.重启或执行`sudo udevadm control --reload`
  
  3.进行刷机

