# 知识体系中控面板案例—基于OpenHarmony最新代码

下载知识体系代码仓库：https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master 

将 dev\team_x\smart_cenctrl_board文件夹下面的 demo_cenctrl 文件夹复制到 OpenHarmony代码这个目录下：applications\sample\camera 

复制完后代码目录如下：

![image-20220507133349163](image/image-20220507133349163.png)



修改文件**build/lite/components/applications.json**

增加如下内容：

![image-20220507133645479](image/image-20220507133645479.png)

    {
      "component": "demo_cenctrl",
      "description": "hello world samples.",
      "optional": "true",
      "dirs": [
        "applications/sample/camera/demo_cenctrl"
      ],
      "targets": [
        "//applications/sample/camera/demo_cenctrl:cenctrl"
      ],
      "rom": "",
      "ram": "",
      "output": [],
      "adapted_kernel": [ "liteos_a" ],
      "features": [],
      "deps": {
        "components": [],
        "third_party": []
      }
    },



修改文件**vendor/hisilicon/hispark_taurus/config.json** 增加如下内容

![image-20220507133800560](image/image-20220507133800560.png)

`{ "component": "demo_cenctrl", "features":[] },`





开始编译即可



编译成功后，复制生成的 so文件到applications\sample\camera\demo_cenctrl目录

`cp out/hispark_taurus/ipcamera_hispark_taurus/mksh_rootfs/usr/lib/libcenctrl.so applications/sample/camera/demo_cenctrl/`

打包压缩：

`cd applications/sample/camera/demo_cenctrl/`

`zip cenctrl.zip libcenctrl.so res/ config.json`

将 cenctrl.zip 重命名未 cenctrl.hap