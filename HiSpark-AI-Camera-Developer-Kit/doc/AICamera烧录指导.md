# AI Camera烧录指导

5月9号之后的镜像，hi3516 第一次烧写包含ramdisk.img的镜像前需要先进行一次擦除全器件，烧写成功后，后续再烧不需要擦除。

## **烧录工具的下载及驱动的安装**

烧录工具下载连接：https://gitee.com/hihope_iot/docs/tree/master/HiSpark-AI-Camera-Developer-Kit/Software/tools

**1.** ***\*首先下载“烧录工具”这个压缩包\****

![img](image/wps4.jpg) 

 

**2.** ***\*解压完成后会有以下几个文件，分别是USB驱动，USB转串口驱动，hitool烧录工具和一个HiUSB补丁文件\****

![img](image/wps5.jpg) 

 

**3.** ***\*安装USB驱动\****

解压HiUSBBurnDriver这个压缩包。

如果你电脑是第一次安装 HiTool 工具，双击 HiUSBBurnDriver\ InstallDriver.exe 安装 USB 驱动，再双击  烧录工具\usb-patch-for-win8win10.reg，进行 USB 补丁的安装。

![img](image/wps6.jpg) 

![img](image/wps7.jpg) 

 

 

**4.** ***\*安装CH340驱动\****

连接好 CH340G 串口线：先将 4pin 串口连接在核心板的 4pin 接口位置。CH340G 串口线接线如下图所示。

![img](image/wps8.jpg) 

CH340G 的实物图如下图所示

![img](image/wps9.jpg) 

请将 USB 的一头插入电脑上，若你的电脑是第一次使用 CH340G 的串口线，需要安 

装驱动，双击USB-to-Serial Comm Port.exe 驱动程序，如果在电脑的设备管理器上能看到设备即可

![img](image/wps10.jpg) 

 

![img](image/wps11.jpg) 

 

## **烧录前的hitool配置及****烧录****固件的准备**

**1.** ***\*解压Hi3516-HiTool这个hitool工具的压缩包\****

 

![img](image/wps12.jpg) 

**2.** ***\*直接双击\**** 烧录工具 Hi3516-HiTool\HiTool.exe文件即可。



![img](image/wps13.jpg)

 

![img](image/wps14.jpg) 

 

 

 

 

**3.** ***\*选择好正确的芯片类型：这里是\**** ***\*Hi3516DV300\****

![img](image/wps15.jpg) 

 

 

**4.** ***\*先选择串口烧录，然后接上串口线，点下刷新，让串口那一栏有COM口占用。该步骤只是为了解决待会USB烧录时出现的一个bug，USB烧录实际上并没有用到串口。\****

![img](image/wps16.jpg) 

 

**5.** ***\*选择“emmc”烧录和“USB口”烧录方式\****

![img](image/wps17.jpg) 

 

**6.** ***\*选择已有的分区表文件或者自己在下面配置\****

![img](image/wps18.jpg) 

 

自己配置的话，如下图所示，此处以OpenHarmony3.1Release版本的固件为例，第二个红框内就是固件的具体位置，第三个和第四个就是开始地址和长度，最后选中要烧录的固件。

 

 

## **HiTool的烧录**

**1.** ***\*先将板子断电，点击hitool工具的烧写按钮\****

![img](image/wps19.jpg) 

 

**2.** ***\*按住主板上面的update按键\****，在串口旁边。

  ***\*然后使用USB上电\****（注意：除了特殊硬件版本，其他的像淘宝渠道购买的都是板子后面的凹槽处），使用的是后面凹槽处的USB口，然后等Hitool工具进入烧录模式后，松开update键。

 

![img](image/wps20.jpg) 

USB烧录口的位置，如果烧录失败，可以换另一个口重新尝试一次

![img](image/wps21.jpg) ![img](image/wps22.jpg)

 

**3.** ***\*进入烧录模式，开始烧录\****

![img](image/wps23.jpg) 

**4.** ***\*烧录完成后，若没有正常启动，需要输入启动参数\**** 

打开hitool工具的串口终端，根据自己实际的COM口去操作，第④步就是打开终端

![img](image/wps24.jpg) 

 

**5.** ***\*打开串口终端后，重新上电\****

在倒计时结束之前，按几下回车键进入uboot模式，然后将修改启动参数的命令粘贴进去

![img](image/wps25.jpg) 

 

 

 

***\*通常情况下，3516支持3种系统类型：\****

每种系统的固件不一样，启动参数也不一样。

 

 

**（1）小型系统（liteos-a内核）**

![img](image/wps26.jpg) 

setenv bootcmd "mmc read 0x0 0x80000000 0x800 0x4800; go 0x80000000";  

setenv bootargs "console=ttyAMA0,115200n8 root=emmc fstype=vfat rootaddr=10M rootsize=30M rw"; 
 
saveenv 



**（2）小型系统（linux内核）**![img](image/wps27.jpg) 

`setenv bootargs "mem=128M console=ttyAMA0,115200 root=/dev/mmcblk0p3 rw rootfstype=ext4 rootwait blkdevparts=mmcblk0:1M(boot),9M(kernel),50M(rootfs),50M(userfs),1024M(userdata)"` 

`setenv bootcmd "mmc read 0x0 0x82000000 0x800 0x4800;mw 0x10FF0044 0X600;mw 0x120D2010 0x00000000;mw 0x120D2400 0x000000ff;mw 0x120D2010 0x00000000;bootm 0x82000000"`  

`saveenv` 

**（3）标准系统**![img](image/wps28.jpg) 

3.0 LTS 和 3.1 Beta版本：

`setenv bootargs 'mem=640M console=ttyAMA0,115200 mmz=anonymous,0,0xA8000000,384M clk_ignore_unused androidboot.selinux=permissive skip_initramfs rootdelay=10 init=/init root=/dev/mmcblk0p5 rootfstype=ext4 rw blkdevparts=mmcblk0:1M(boot),15M(kernel),20M(updater),1M(misc),3307M(system),256M(vendor),-(userdata)'  `

`setenv bootcmd "mmc read 0x0 0x80000000 0x800 0x4800; bootm 0x80000000"  `

`saveenv`  

 3.1 release之后的版本：

`setenv bootargs 'mem=640M console=ttyAMA0,115200 mmz=anonymous,0,0xA8000000,384M clk_ignore_unused androidboot.selinux=permissive skip_initramfs rootdelay=5 hardware=Hi3516DV300 default_boot_device=soc/10100000.himci.eMMC init=/init root=/dev/ram0 blkdevparts=mmcblk0:1M(boot),15M(kernel),20M(updater),2M(misc),3307M(system),256M(vendor),-(userdata)'`

`setenv bootcmd "mmc read 0x0 0x80000000 0x800 0x4800; bootm 0x80000000"  `

`saveenv`  

 

 

 

 