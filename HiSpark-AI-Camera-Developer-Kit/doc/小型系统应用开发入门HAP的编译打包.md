

# 五、小型系统应用开发入门HAP的编译打包

编写HAP包

JS API参考地址：https://developer.harmonyos.com/cn/docs/documentation/doc-references/ts-universal-events-click-0000001111581270

\1. 下载DevEco Studio

\2. 安装DevEco Studio

\3. 新建项目

![img](image/wps63.jpg) 

\4. 填写路径

![img](image/wps64.jpg) 

\5. 修改实验代码

![img](image/wps65.jpg) 

\6. 编译生成和预览

![img](image/wps66.jpg) 

![img](image/wps67.jpg) 

\7. 打成HAP包

![img](image/wps68.jpg) 

HAP包位置

![img](image/wps69.jpg) 