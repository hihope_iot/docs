# 安装HAP包

## 1.搭建NFS	服务器

https://ost.51cto.com/posts/3078 

使用NFS服务器来传输文件，软件名称：haneWIN NFS，需要在d盘新建文件夹。

![img](image/wps70.jpg) 

 

 

 

![img](image/wps71.jpg) 

然后防火墙打开端口

![img](image/wps72.jpg) 

进入NFS服务器的目录重启所有服务，可以使用NFS客户端测试连接

![img](image/wps73.jpg) 

有时候需要重启电脑才能生效。



## 2.Hi3516挂载NFS

先保证开发板跟电脑在同一个网段，可以先操作板子WiFi连接到路由器

串口登陆Hi3516。

挂载命令是mount，参数如下：

```markup
OHOS # mount
mount [DEVICE] [PATH] [NAME]1.2.1.2.1.2.1.2.
```

复制

DEVICE是要挂载的远程设备目录，格式为[IP]:[目录]

PATH是挂载本地目录

NAME是挂载名称

这里对应的windows目录是c:\public，我的windows端IP是192.168.1.3，因此第一个参数就是192.168.1.3:/c/public

先创建挂载点目录，再做挂载。命令如下：

```markup
OHOS # mkdir /nfs
OHOS # mount 192.168.1.3:/c/public /nfs nfs
Mount nfs on 192.168.1.3:/c/public, uid:0, gid:0
Mount nfs finished.
OHOS # ls /nfs
Directory /nfs:
-rw-r--r-- 8        u:0     g:0     111.txt
OHOS # cat /nfs/111.txt
OHOS # asdfadsf
OHOS 
```



**安装hap**

安装hap需要使用bm工具，在这个目录：\\out\ipcamera_hi3516dv300\dev_tools\bin

与hap文件一起，拷贝到c:\public下

然后先禁用签名校验，再安装hap，命令如下：

```c
OHOS # cd /nfs
OHOS # ./bm set -s disable
OHOS # 01-01 02:11:31.924 20 61 I 01800/Samgr: Initialize Registry!
success
01-01 02:11:31.925 5 32 D 01800/Samgr: Judge Auth<bundlems, BmsInnerFeature> ret:0
01-01 02:11:31.925 5 32 D 01800/Samgr: Find Feature<bundlems, BmsInnerFeature> id<38, 0> ret:0
01-01 02:11:31.925 20 61 I 01800/Samgr: Create remote sa proxy[0x256136f0]<bundlems, BmsInnerFeature> id<38,0>!
01-01 02:11:31.925 5 35 I 00000/(null): current sign mode is 0

OHOS # ./bm install -p air-quality.hap
...
resultMessage is install success !
...
```